#!/usr/bin/env python
#ULOHA 3 -------------------------------------------------------------
from operator import add
class Polynomial:
    def __init__(self, *args, **kwargs):
        if args and isinstance(args[0], list):
            self.list = args[0]
            self.length = len(args[0])
        elif args:
            self.list = args
            self.length = len(args)
        else:
            self.list = [kwargs.get(x, 0) for x in ('x0', 'x1', 'x2', 'x3', 'x4', 'x5', 'x6')]
            self.length = len([kwargs.get(x, 0) for x in ('x0', 'x1', 'x2', 'x3', 'x4', 'x5', 'x6')])
#       for self.length in self.list:
#          print(self.list[self.length])

    def __str__(self):
        s = ''
        for i, x in reversed(list(enumerate(self.list))):
            if x:
                if x > 0:
                    s += '+'
                s += str(x)
                if i > 0:
                    s += 'x'
                if i > 1:
                    s += '^' + str(i)
        return '0' if not s else s.lstrip('+')


    def __add__(self, other):
        if self.length > other.length:
            x = self.length - other.length
            other_tmp = list(other.list)
            for y in range(0, x):
                other_tmp.append(0)
            other.list = tuple(other_tmp)
            other.list = [sum(i) for i in zip(self.list, other.list)]
            return other.__str__()
        else:
            x = other.length - self.length
            self_tmp = list(self.list)
            for y in range(0,x):
                self_tmp.append(0)
            self.list = tuple(self_tmp)
            self.list = [sum(i) for i in zip(self.list, other.list)]
            return self.__str__()

        #other.list = [sum(i) for i in zip(self.list, other.list)]
        #return other.__str__()
        #return [sum(i) for i in zip(self.list, other.list)]

#   def __add__(self, other):
#
#       if self.length > other.length:
#          self[:other.length] += other
#         ret = self
#    else:
#       other[:self.length] += self
#      ret = other
# return ret

#    def __add__(self,other):
#        '''
#            Kvoli "ZIP", ktory bere dlzku kratsieho zoznamu, kratsi zoznam doplnim nulami.
#        '''
#        if self.length > other.length:
#            x = self.length - other.length
#            for y in range(0,x):
#                other.list.append(0)
#        else:
#            x = other.length - self.length
#            for y in range(0,x):
#                self.list.append(0)
#
#        return [sum(i) for i in zip(self.list,other.list)]


    def __pow__(self, other):

        return self.__str__()




test_val = Polynomial(1,2,3,1)
test_val2 = Polynomial([1,2,3,1])
#test_val = Polynomial(x0=1, x1=-3, x3=2, x4=4)

#test_val2 = Polynomial([1,1,1,1])
#print (test_val)
#print (test_val.list)
#print (test_val.length)

print(Polynomial(-1, 1) ** 2)

#print (test_val)
